#!/bin/sh

# Inputs
for ARGUMENT in "$@"
do
   KEY=$(echo "$ARGUMENT" | cut -f1 -d=)
   VALUE=$(echo "$ARGUMENT" | cut -f2 -d=)

   export "$KEY"="$VALUE"
done

if [ -z "${SERVER_ID}" ]; then >&2 echo "SERVER_ID is unset" && export error=1; fi
# if [ -z "${VOLUME_ID}" ]; then >&2 echo "VOLUME_ID is unset" && export error=1; fi
if [ -z "${APIKEY_ID}" ]; then >&2 echo "APIKEY_ID is unset" && export error=1; fi
if [ -z "${SECRET}" ]; then >&2 echo "SECRET is unset" && export error=1; fi
if [ -z "${AUTH_URL}" ]; then >&2 echo "AUTH_URL is unset" && export error=1; fi
if [ -z "${NOVA_URL}" ]; then >&2 echo "NOVA_URL is unset" && export error=1; fi

if ! [ -z ${error} ]; then exit 1; fi


# POST Body
generate_post_data() {
  cat <<EOF
  {
    "auth": {
        "identity": {
            "methods": [
                "application_credential"
            ],
            "application_credential": {
                "id": "$APIKEY_ID",
                "secret": "$SECRET"
            }
        }
    }
}
EOF
}

# Authentication
get_token() {
  curl -i \
    -H "Content-Type: application/json" \
    -X POST --data "$(generate_post_data)" "$AUTH_URL/auth/tokens" --silent | grep X-Subject-Token | awk '{print $2}' | sed 's/\t\r\n//g'
}

# Detaching Volume

# Note that the URL is naive. For openstack clusters outside of SURF this should be parsed from the response of the
# authentication request.
detach_volume() {
  echo "Detaching $1";
  curl -g -i -X DELETE \
    -H "Accept: application/json" \
    -H "X-Auth-Token: ${TOKEN}" \
    -H "X-OpenStack-Nova-API-Version: 2.21" \
    "${NOVA_URL}:8774/v2.1/servers/${SERVER_ID}/os-volume_attachments/$1"
}
TOKEN="$(get_token)"
# detach_volume "$VOLUME_ID"
if ! [ -z "${VOLUME_ID}" ]; then
  for VOLUME in $VOLUME_ID; do
      detach_volume "$VOLUME"
      sleep 10
  done ;
fi
