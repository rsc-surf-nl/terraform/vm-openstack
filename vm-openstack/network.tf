locals {
  security_group_rules = distinct(concat(
    [for item in var.security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")],
    [for item in var.config_security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")],
    [for item in local.external-network-rules : replace(replace(item, " immutable", ""), " mutable", "")]
  ))
}

resource "openstack_networking_network_v2" "network" {
  count          = local.external-network != null ? 0 : 1
  name           = "${var.workspace_id}-network"
  admin_state_up = "true"
  tags = [
    "${var.workspace_id}-network",
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]
}

resource "openstack_networking_subnet_v2" "subnet" {
  count           = local.external-network != null ? 0 : 1
  name            = "${var.workspace_id}-subnet"
  network_id      = openstack_networking_network_v2.network[0].id
  cidr            = var.cidr
  ip_version      = 4
  dns_nameservers = var.nameservers
  tags = [
    "${var.workspace_id}-subnet",
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]
}

resource "openstack_networking_router_v2" "router" {
  count               = local.external-network != null ? 0 : 1
  name                = "${var.workspace_id}-router"
  external_network_id = local.external-floating-ip != null ? local.external-floating-ip.router_external_network_id.value : var.router_external_network_id
  tags = [
    "${var.workspace_id}-router",
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]
}

resource "openstack_networking_router_interface_v2" "router_interface" {
  count     = local.external-network != null ? 0 : 1
  router_id = openstack_networking_router_v2.router[0].id
  subnet_id = openstack_networking_subnet_v2.subnet[0].id
}

resource "openstack_networking_secgroup_v2" "secgroup" {
  name        = "${var.workspace_id}-secgroup"
  description = "${var.workspace_id}-secgroup"
  tags = [
    "${var.workspace_id}-secgroup",
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule" {
  count             = length(local.security_group_rules)
  direction         = split(" ", local.security_group_rules[count.index])[0] == "in" ? "ingress" : "egress"
  ethertype         = "IPv4"
  protocol          = split(" ", local.security_group_rules[count.index])[1]
  port_range_min    = split(" ", local.security_group_rules[count.index])[2]
  port_range_max    = split(" ", local.security_group_rules[count.index])[3]
  remote_ip_prefix  = split(" ", local.security_group_rules[count.index])[4]
  security_group_id = openstack_networking_secgroup_v2.secgroup.id
}

# Add floating ip if we don't have a list of predefined ips to attach
resource "openstack_networking_floatingip_v2" "floatip" {
  count = local.external-floating-ip != null || var.floating_ip_pool == "" ? 0 : 1
  pool  = var.floating_ip_pool
  tags = [
    var.workspace_id,
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]
}

resource "openstack_networking_port_v2" "instance_network_port" {
  network_id = local.external-network != null ? local.external-network.id.value : openstack_networking_network_v2.network[0].id
  fixed_ip {
    subnet_id = local.external-network != null ? local.external-network.subnet_id.value : openstack_networking_subnet_v2.subnet[0].id
  }
  security_group_ids = [openstack_networking_secgroup_v2.secgroup.id]
}

resource "openstack_networking_floatingip_associate_v2" "floatip" {
  count       = local.external-floating-ip != null || var.floating_ip_pool == "" ? 0 : 1
  floating_ip = openstack_networking_floatingip_v2.floatip[0].address
  port_id     = openstack_networking_port_v2.instance_network_port.id
}

resource "openstack_networking_floatingip_associate_v2" "external-floatip" {
  count       = local.external-floating-ip != null ? 1 : 0
  floating_ip = local.external-floating-ip.address.value
  port_id     = openstack_networking_port_v2.instance_network_port.id
}
