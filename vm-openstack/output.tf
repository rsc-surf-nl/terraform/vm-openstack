output "vm_id" {
  value = openstack_compute_instance_v2.vm-openstack.id
}


output "id" {
  value = openstack_compute_instance_v2.vm-openstack.id
}

output "ip" {
  value = local.external-floating-ip != null ? local.external-floating-ip.address.value : (var.floating_ip_pool != "" ? openstack_networking_floatingip_v2.floatip[0].address : openstack_compute_instance_v2.vm-openstack.access_ip_v4)
}

output "local_ip" {
  value = local.external-network != null ? openstack_networking_port_v2.instance_network_port.all_fixed_ips[0] : null
}

output "instance_user" {
  value = var.instance_user
}

output "instance_password" {
  value     = random_password.password.result
  sensitive = true
}

output "network_secgroup_id" {
  value = openstack_networking_secgroup_v2.secgroup.id
}

output "secgroup_id" {
  value = openstack_networking_secgroup_v2.secgroup.id
}

output "private_key" {
  value     = openstack_compute_keypair_v2.keypair.private_key
  sensitive = true
}

output "public_key" {
  value = openstack_compute_keypair_v2.keypair.public_key
}

output "image_id" {
  value = var.image_id != "" ? var.image_id : data.openstack_images_image_v2.base_image.id
}

# Mark support for actions
output "terraform_script_version" {
  value = 3.2
}

# Accounting outputs
output "boot_volume_type" {
  value = var.boot_volume_type
}

output "boot_volume_size" {
  value = var.boot_volume_size
}

output "min_os_disk_size" {
  value = var.min_os_disk_size
}

output "flavor_name" {
  value = var.flavor_name
}
