variable "project_id" {}
variable "project_name" {}
variable "user_name" {
  description = "Not the username, but the application ID for application credential authentication."
}
variable "password" {
  description = "Not the password, but the application secret for application credential authentication."
}
variable "region" {}
variable "auth_url" {}
variable "cidr" {}
variable "router_external_network_id" {}
variable "floating_ip_pool" {
  default = ""
}
variable "image_name" {}
variable "flavor_name" {}
variable "instance_name" {
  default = "rsc-instance"
}
variable "instance_user" {}
variable "instance_tag" {}
variable "boot_volume_size" {
  type    = number
  default = 50
}

variable "boot_volume_type" {
  default = ""
}

variable "security_group_rules" {
  type = list(string)
}

variable "config_security_group_rules" {
  type    = list(string)
  default = []
}

variable "min_os_disk_size" {
  type    = number
  default = 20
}

variable "external_volume_ids" {
  type    = list(string)
  default = []
}
variable "image_id" {
  default = ""
}
variable "external_volumes" {
  default = "[]"
}
variable "external_floating_ips" {
  default = "[]"
}
variable "external_networks" {
  default = "[]"
}
variable "nameservers" {
  type        = list(string)
  description = "A list of nameservers to use"
}

# SRC automatic variables are added explicitly to be self-contained
variable "application_type" {
  type        = string
  description = "The application type (eg. 'Compute', 'Storage' etc.)"
  default     = "Compute"
}
variable "cloud_type" {
  type        = string
  description = "Cloud type (eg. 'AWS', 'Openstack', 'Azure' etc.)"
  default     = "Openstack"
}
variable "co_id" {
  type        = string
  description = "The ID of the associated CO"
  default     = "unset"
}
variable "resource_type" {
  type        = string
  description = "The resource type (eg. 'VM', 'Storage-Volume' etc.)"
  default     = "VM"
}
variable "subscription" {
  type        = string
  description = "The (SRC) subscription under which this resources is created"
  default     = "unset"
}
variable "subscription_group" {
  type        = string
  description = "The subscription group to which the `subscription` belongs"
  default     = "unset"
}
variable "wallet_id" {
  type        = string
  description = "The ID of the associated wallet"
  default     = "unset"
}
variable "workspace_fqdn" {
  type        = string
  description = "The FQDN assigned to this workspace"
  default     = "example.surf-hosted.nl"
}
variable "workspace_id" {
  type        = string
  description = "The ID of this workspace"
  default     = "unset"
}
