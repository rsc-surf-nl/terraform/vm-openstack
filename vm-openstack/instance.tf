locals {
  external-volumes     = tolist(jsondecode(var.external_volumes))
  external-floating-ip = length(tolist(jsondecode(var.external_floating_ips))) != 0 ? tolist(jsondecode(var.external_floating_ips))[0] : null
  external-network     = length(tolist(jsondecode(var.external_networks))) != 0 ? tolist(jsondecode(var.external_networks))[0] : null
  external-network-rules = try(
    local.external-network != null ? local.external-network.secgroup_rules.value : [],
    local.external-network != null ? jsondecode(local.external-network.secgroup_rules.value) : [],
  )
  dns_ipv4  = local.external-floating-ip == null ? (var.floating_ip_pool != "" ? openstack_networking_floatingip_v2.floatip[0].address : openstack_compute_instance_v2.vm-openstack.access_ip_v4) : local.external-floating-ip.address.value
  split_url = split(":", var.auth_url)
  nova_host = join("", [local.split_url[0], ":", local.split_url[1]])
}

data "openstack_images_image_v2" "base_image" {
  name        = var.image_name
  most_recent = true
}

resource "openstack_compute_keypair_v2" "keypair" {
  name = "${var.workspace_id}-keypair"
}

resource "random_password" "password" {
  length           = 24
  special          = true
  override_special = "_%@)"
}

resource "openstack_blockstorage_volume_v3" "boot-volume" {
  name        = "${var.workspace_id}-boot-volume"
  size        = max(var.min_os_disk_size, var.boot_volume_size)
  volume_type = var.boot_volume_type
  image_id    = var.image_id != "" ? var.image_id : data.openstack_images_image_v2.base_image.id
  metadata = {
    workspace_id       = var.workspace_id
    subscription       = var.subscription
    application_type   = var.application_type
    resource_type      = var.resource_type
    cloud_type         = var.cloud_type
    subscription_group = var.subscription_group
    volume_type        = var.boot_volume_type
  }
}

resource "openstack_compute_instance_v2" "vm-openstack" {
  name                = var.instance_name
  flavor_name         = var.flavor_name
  key_pair            = openstack_compute_keypair_v2.keypair.name
  tags                = [var.subscription, var.instance_tag, var.workspace_id]
  stop_before_destroy = true
  user_data           = "#cloud-config\n  \nmounts:\n - [ephemeral0, null]"
  network {
    port = openstack_networking_port_v2.instance_network_port.id
  }
  block_device {
    source_type           = "volume"
    uuid                  = openstack_blockstorage_volume_v3.boot-volume.id
    boot_index            = 0
    delete_on_termination = true
    destination_type      = "volume"
  }

  depends_on = [openstack_networking_subnet_v2.subnet, openstack_blockstorage_volume_v3.boot-volume]

  metadata = {
    admin_pass         = random_password.password.result,
    workspace_id       = var.workspace_id
    subscription       = var.subscription
    application_type   = var.application_type
    resource_type      = var.resource_type
    cloud_type         = var.cloud_type
    subscription_group = var.subscription_group
  }
}

resource "openstack_compute_volume_attach_v2" "external-volumes-attach" {
  count       = length(local.external-volumes)
  instance_id = openstack_compute_instance_v2.vm-openstack.id
  volume_id   = local.external-volumes[count.index].id.value
}

resource "null_resource" "sh" {
  depends_on = [openstack_compute_volume_attach_v2.external-volumes-attach]
  triggers = {
    instance_id2  = openstack_compute_instance_v2.vm-openstack.id
    volume_id2    = join(" ", "${local.external-volumes.*.id.value}")
    apikey_id     = sensitive(var.user_name)
    apikey_secret = sensitive(var.password)
    auth_url      = sensitive(var.auth_url)
    nova_url      = sensitive(local.nova_host)

  }
  provisioner "local-exec" {
    when = destroy
    environment = {
      SERVER_ID = self.triggers.instance_id2
      VOLUME_ID = self.triggers.volume_id2
      APIKEY_ID = sensitive(self.triggers.apikey_id)
      SECRET    = sensitive(self.triggers.apikey_secret)
      AUTH_URL  = sensitive(self.triggers.auth_url)
      NOVA_URL  = sensitive(self.triggers.nova_url)

    }
    command = "./detach_volumes.sh"
  }
}
